\documentclass[12pt]{article}
\usepackage{amsmath,amssymb,amsfonts,amsthm,enumitem,verbatim,graphicx,bbm}
\usepackage{fullpage}
\newcommand{\mb}[1]{\mathbf{#1}}

\begin{document}
\noindent \textbf{Bayesian Inference}   \hfill Booth/UChicago  \\
\textbf{Homework 5} \hfill RBG/Spring 2016 \\  

\noindent Solutions. \\

\begin{enumerate}
  \item Calculate the distribution of $x_1$ conditional on $x_2$ by treating the $x_2$ term in the joint distribution as known:
  \begin{align*}
  &\pi(x_1|x_2) \propto exp\left(-\frac{1}{2}(x_1-1)^2(x_2-2)^2\right) \sim N\left(1, \frac{1}{(x_2-2)^2}\right). \\
  \intertext{Similarly,}  
  &\pi(x_2|x_1) \sim N\left(2, \frac{1}{(x_1-1)^2}\right)
  \end{align*}
  So both full conditionals are normal, and therefore easy to sample from.  But $\pi(x_1,x_2)$ is improper, i.e.
  \begin{equation*}
  \int_{-\infty}^\infty\int_{-\infty}^\infty \pi (x_1, x_2)dx_1dx_2 = \infty
  \end{equation*}
  Thus, $\pi$ is  not a  density  and so  using a  Gibbs sampler  to sample  from it  makes  no sense.
  \item 
  \begin{enumerate}[label=(\alph*)]
    \item $\theta_A$ and $\theta_B$ are clearly not independent. It is easy to see that one is defined in terms of the other: $\theta_B=\theta_A\gamma$.  This is a sensible prior when the parameter(s) describe similar populations which are largely identical except for a small characteristic. Men of similar age but differing in education is one example.
    \item To  derive  the  conditional(s)  it  helps  to  first  obtain  an  expression  for  the  joint posterior.
    \begin{align*}
    \pi(\theta, \gamma|\mb{y}_A,\mb{y}_B) &\propto \pi(\mb{y}_A, \mb{y}_B|\theta, \gamma) \times \pi(\theta, \gamma) = \pi(\mb{y}_A|\theta) \times \pi (\mb{y}_B|\theta, \gamma) \times \pi(\theta, \gamma) \\
    &\propto \prod_{i=1}^{n_A}\theta^{y_{Ai}}e^{-\theta} \times \prod_{j=1}^{n_B}(\theta\gamma)^{y_{Bj}}e^{-\theta\gamma}\times \theta^{\alpha_{\theta}-1}e^{-\beta_{\theta}}\gamma^{\alpha_{\gamma}-1}e^{-\beta_{\gamma}\gamma} \\
    &\propto e^{-\theta n_{A}}\theta^{\sum_iy_{Ai}} \times e^{-\theta\gamma n_B}(\theta\gamma)^{\sum_j y_{Bj}}\times \theta^{\alpha_{\theta}-1}e^{-\beta_\theta \theta}\gamma^{\alpha_\gamma - 1}e^{-\beta_\gamma \gamma}.
    \end{align*}
    Then the conditionals can be derived by treating the parameters conditioned upon as constants of proportionality. \\
    We have
    \begin{align*}
    \pi(\theta|\gamma, \mb{y}_A, \mb{y}_B) &\propto e^{-\theta(n_A+\gamma n_B + \beta_\theta)}\theta^{\sum_iy_{Ai}+\sum_jy_{Bj} + \alpha_\theta-1}, \\
    \text{so} \quad \lbrace \theta | \gamma, \mb{y}_A, \mb{y}_B \rbrace &\sim G(\sum\nolimits_i y_{Ai} + \sum\nolimits_j y_{Bj} + \alpha_\theta,n_A+\gamma n_B+\beta_\theta)
    \end{align*}
    \newpage
    \item Similarly
    \begin{align*}
    \pi(\theta|\gamma, \mb{y}_A, \mb{y}_B) &\propto e^{-\gamma(\theta_{n_B}+\beta_\gamma)}\gamma^{\sum_jy_{Bj}+\alpha_\gamma-1}, \\
    \text{so} \quad \lbrace \theta | \gamma, \mb{y}_A, \mb{y}_B \rbrace &\sim G(\sum_jy_{Bj}+\alpha_\gamma, \theta n_B + \beta_\gamma).
    \end{align*}
    \item See {\sf R} code in \texttt{birth\_edu\_male\_gibbs.R}. 
    \begin{figure}[ht!]
    \includegraphics[width=0.4\textwidth]{figures/tBmtA.pdf} \centering
    \caption{Posterior expectation of $\theta_B -\theta_A$ as a function of $a_\gamma -b_\gamma$.}
    \label{fig:1}
    \end{figure}
    
    Figure  \ref{fig:1}  shows  the  resulting  posterior  expectations  as  a  function  of  the  prior values for $\gamma$.  The $\gamma$ parameter links $\theta_A$ to $\theta_B$, and the prior mean is $\alpha_\gamma/\beta_\gamma = 1$ for all choices of these values.  Notice that the prior sample size increases with $b_\gamma$.  So as these values increase, the prior for $\gamma$ becomes more concentrated near one, forcing the populations of $\theta_A$ and $\theta_B$ to become more similar \textit{a priori}, and simultaneously contribute more weight to the prior in the posterior (relative to the data via the likelihood).  However, we see from the figure that the prior sample size must become very large before we would conclude that $\theta_B\ngeq \theta_A$ given this particular data.
  \end{enumerate}
  \item The MH acceptance ratio is
  \begin{equation*}
  \alpha(\theta, \phi) = min\left( 1, \frac{\pi(\phi)q(\phi, \theta)}{\pi(\theta)q(\theta, \phi)} \right).
  \end{equation*}
  Here
  \begin{equation*}
  \pi(\theta)=\frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{1}{2\sigma^2}\theta^2}, \quad \text{and} \quad q(\theta, \phi) = \frac{1}{\sqrt{2\pi \tau^2}}exp\left( -\frac{1}{2\tau^2}(\phi - \alpha\theta)^2\right).
  \end{equation*}
  Therefore
  \begin{equation*}
  \frac{\pi(\phi)q(\phi, \theta)}{\pi(\theta)q(\theta, \phi)} = exp \left( -\frac{1}{2}(\phi^2 -\theta^2) \left( \frac{1}{\sigma^2} + \frac{a^2 -1}{\tau^2} \right) \right)
  \end{equation*}
  \newpage
  This sampler never rejects if $\alpha(\theta,\phi)\equiv 1$ for all $\theta$ and $\phi$.  Observe that this happens if and only if $\frac{1}{\sigma^2}+ \frac{a^2-1}{\tau^2} = 0$, i.e., when $\tau^2 = \sigma^2( 1 - a^2)$.  Note this also implies that we must have $\tau^2 > \sigma^2$.
  \item See \texttt{clouds.R} for {\sf R} code.
  \begin{enumerate}[label=(\alph*)]
  \item Figure  \ref{fig:2}  shows  histograms  of  the  two  sets  of  clouds,  and  histograms  of  their \linebreak logged values.  The original values are all positive, and there is a heavy right-hand \\
  \begin{figure}[ht!]
  \includegraphics[width=0.6\textwidth]{figures/clouds.pdf} \centering
  \caption{Histograms of the original data and logged values.}
  \label{fig:2}
  \end{figure}
  
  tail, both of which preclude (reasonable) modeling with a normal distribution.  It may be sensible to model the logged values with a normal distribution though, as these are much more symmetric and have less of a heavy tail.  That may lead to an interesting comparison.
\item The  code  uses  uses  RW  normal  proposals  centered  around  the  previous  values with $\sigma^2_a = 0.25$ for both cloud's "$a$" parameters, and $\sigma^2_b = 0.00001$ for their "$b$" parameters.  These values were found by several pilot runs and inspecting ESSs and autocorrelations. \\
For the unseeded clouds (the "none" column), with $u$ subscripts, in round $s+ 1$ we propose $a'_u \sim \mathcal{N}(a_u^{(s)}, 0.25)$. Draw $u \sim$ Unif$(0,1)$. Then, if 
\begin{equation*}
u < \frac{\prod_{i=1}^{26}G(y_{u,i}; a'_u, b_u^{(s)})}{\prod_{i=1}^{26}G(y_{u,i}; a^{(s)}_u, b_u^{(s)})}, \quad \text{(G is the Gamma pdf)}
\end{equation*}
we take $a_u^{(s+1)} = a'_u$; otherwise $a_u^{(s+1)} = a^{(s)}_u$. Then we propose $b'_u \sim \mathcal{N}(b_u^{(s)}, 0.00001)$. Draw $u \sim$ Unif$(0,1)$. Then, if
\begin{equation*}
u < \frac{\prod_{i=1}^{26}G(y_{u,i}; a^{(s+1)}_u, b'_u)}{\prod_{i=1}^{26}G(y_{u,i}; a^{(s+1)}_u, b_u^{(s)})}, 
\end{equation*}
we take $b_u^{(s+1)} = b'_u$; otherwise $b_u^{(s+1)} = b^{(s)}_u$. \\
The procedure is exactly the same for the seeded clouds, but with $s$ subscripts. Both chains were initialized at $(a,b) = (2,0.015)$. 

\begin{figure}[ht!]
\includegraphics[width=0.7\textwidth]{figures/cloud_mwing.pdf} \centering
\caption{Traces of samples from the (independent) posteriors.}
\label{fig:3}
\end{figure}
Figure \ref{fig:3} shows the traces from the four parameters - two independent sets from each cloud type. The chains quickly converge their respective target distributions and seem to mix well.
\item As in Question 2, it helps to have an expression for the full posterior to derive a conditional.  Lets consider the unseeded clouds since the derivation for seeded clouds is similar.  The $u$ subscript will be omitted for brevity.
\begin{equation*}
\pi (\alpha, \beta | \gamma) \propto \prod_{i=1}^n \frac{\beta^\alpha}{\Gamma(\alpha)}y_i^{\alpha -1} e^{-\beta y_i} \times \lambda_\alpha e^{-\lambda_\alpha \alpha }\lambda_\beta e^{-\lambda_\beta}
\end{equation*}
Therefore, 
\begin{align*}
\pi (\beta | \alpha, y) &\propto \beta^{n\alpha}e^{-\beta\sum_{i=1}^n y_i} \times \lambda_\beta e^{\lambda_\beta\beta} \\
&= \beta^{n\alpha}e^{-\beta(\sum_{i=1}^n y_i + \lambda_\beta)}, \\
\text{so} \quad \lbrace \beta_s|\alpha_s, y_s \rbrace &\sim G(n_s\alpha_s + 1, \sum\nolimits_{i=1}^{n_s}y_{si} + \lambda_{\beta_s}) \\
\text{and} \quad \lbrace \beta_u|\alpha_u, y_u \rbrace &\sim G(n_u\alpha_u + 1, \sum\nolimits_{i=1}^{n_u}y_{ui} + \lambda_{\beta_u}).
\end{align*}
\item The  sampler  is  the  same  as  the  one  in  part  (a)  except  that  we  draw  from  the corresponding  gamma  distribution  instead  of  making  RW  proposals  for $\beta_u$ and $\beta_s$. See  the {\sf R} code. We  measure  the  quality  of  the  MC  approximation(s)  by effective sample size (ESS). Below are the results comparing the full MH version [part (a)] to the one that uses Gibbs sampling for $\beta$.  Only the seeded samples are shown; the unseeded ones are similar. \\ 
\texttt{
\begin{tabular}{@{}lrr}
      & a        & b          \\
gibbs & 977.7568 & 1927.7788  \\
mh    & 596.9219 & 543.7367  \\
\end{tabular}
} \\ \\
Notice how both $\alpha$ and $\beta$ marginal chains have better ESSs for the Gibbs version, even though only $\beta$ is being sampled from a full conditional. The lower autocorrelation in the marginal $\beta$ chain reduces the correlation in the marginal $\alpha$ chain too.
\item Consider  two  ways  of  comparing  seeded  to  unseeded  clouds  via  the  posterior distribution:  (1) using the posterior mean(s) $(\alpha/\beta)$, and (2) using the predictive distribution(s).   Both  can  use  the  samples  obtained  from  the  (partial)  Gibbs sampler,  or  the  original  ones.   See  the {\sf R} code.   The  posterior  probability  that the  posterior  mean  of  the  seeded  clouds  (rainfall)  is  greater  than  the  unseeded ones is 0.99, whereas the probability that a new seeded cloud would produce more rainfall than a new unseeded one is lower, at 0.67.  Both suggest strong evidence that seeding clouds leads to a higher level of rainfall.
  \end{enumerate}
\end{enumerate}

\end{document}