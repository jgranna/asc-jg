#!/bin/bash
Rscript -e "rmarkdown::render('hw2_sol.Rmd'); knitr::purl('hw2_sol.Rmd')"

## echo number of chosen parallel processes
echo Run $1 parallel R scripts.

## run command on n cores 
for i in {1..10}; do echo i; done
