#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

static double const log2pi = std::log(2.0 * M_PI);

// [[Rcpp::export]]
double loglik2cpp(arma::mat const &Y, arma::mat const &D, double const &theta){
  using arma::uword;
  uword const m = D.n_rows,
    n = Y.n_rows;
  /* .Machine$double.eps */
  double eps = 2.220446e-16; 
  arma::colvec diagonal(m);
  for (int i = 0; i < m; i++)
  {
    diagonal(i) = sqrt(eps);
  }
  arma::mat Sigma = exp(-D/theta) + diagmat(diagonal);
  arma::mat Schol = arma::chol(Sigma);
  double const ldet = 2*arma::sum(log(Schol.diag()));
  arma::mat const Si = arma::inv(trans(Schol) * Schol);
  double ll = -0.5*n*(m*log2pi + ldet);
  double multconst(n);
  for (int i = 0; i < n; i++) 
  {
    arma::mat mult = Y.row(i) * Si * trans(Y.row(i));
    multconst = as_scalar(mult);
    ll -= 0.5 * multconst;
  }
  return ll; 
}
