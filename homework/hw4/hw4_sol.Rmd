---
title: "Homework 4"
author: "Julian Granna"
date: "11/05/2021"
output: html_document
---

```{r setup, include=FALSE}
# load libraries
library(knitr)
library(mvtnorm)
# load saved data
load("./output/rmd_out.rda")
# set up options
knitr::opts_chunk$set(echo = TRUE)
# remove white margin above graphs
knit_hooks$set(no.main = function(before, options, envir) {
    if (before) par(mar = c(4.1, 4.1, 1.1, 1.1))  # smaller margin on top
})
## ```{r, dev.args = list(bg = 'transparent'), fig.width=4.5, fig.height=4, fig.align="center", no.main=TRUE}
```

## Problem 1

First, I define all variables as suggested in the task:
```{r, eval=FALSE}
thetas <- seq(0.1, 3, length=100)
X <- runif(300, -1, 1)
D <- as.matrix(dist(matrix(X, ncol=1), diag=TRUE, upper=TRUE))
Sigma <- exp(-D) + diag(sqrt(.Machine$double.eps), nrow(D))
Y <- rmvnorm(10000, sigma=Sigma)
source("./R/mvnllik.R")
```

### Part a.
I added the `logliks`-function to the mvnllik.R-file. It is defined as
```{r, eval=FALSE}
logliks <- function(Y, D, theta, verb=1)
{
  n <- nrow(Y)
  m <- ncol(D)
  tlen <- length(theta)
  cat(n, m, tlen)

  ret <- .C("logliks_R",
          n = as.integer(n),
          m = as.integer(m),
          Y = as.double(t(Y)),
          D = as.double(t(D)),
          theta = as.double(theta),
          tlen = as.integer(tlen),
          verb = as.integer(verb),
          llik = double(100),
          DUP = FALSE)

  return(ret$llik)
}
```
I then built the shared objects by running (while the Makevars file is in the same directory)
```{r, eval=FALSE}
R CMD SHLIB -o clect.so mvnllik.c
```
in the terminal and load it into `R` with 
```{r}
dyn.load("./src/clect.so")
```

### Part b. 
I extended the `mvnllik.c` file with OpenMP options. Since all versions are compared later, I excluded a check `#ifdef _OPENMP`, but refer to the OpenMP code with an `_mp` extension instead.

### Part c + d.
I next compare the speed of the four different versions. `out.4` used up to roughly 1500% of CPU. Note: I include screenshots, because a) I ran out of memory and the compiling crashed, and b) because it makes comparison the rmkl easier. It would obviously be better to compile two rmds, one with and one without rmkl.
```{r, eval=FALSE}
system.time( out.1 <- logliks.R(Y, D, thetas, ll=loglik) )
system.time( out.2 <- logliks.R(Y, D, thetas, ll=loglik2) )
system.time( out.3 <- logliks(Y, D, thetas, verb=0) )
system.time( out.4 <- logliks_omp(Y, D, thetas, verb=0) )
```

<img title="a title" alt="Alt text" src="./output/logliks.png">

The \textsf{R} code using the MVN library function was slightly faster than the C code run without OpenMP. `loglik2` was by far the slowest code. The C code with OpenMP was by far the fastest of all codes.
To show that all outputs are identical:
```{r}
(max1 <- thetas[which.max(out.1)])
(max2 <- thetas[which.max(out.2)])
(max3 <- thetas[which.max(out.3)])
(max4 <- thetas[which.max(out.4)])
```
All yield the same maximum and one can also inspect graphically that the functions are identical:
```{r}
library(ggplot2)
dat1 <- data.frame(thetas = rep(thetas, 4), ll = c(out.1, out.2, out.3, out.4), type = rep(c("out.1", "out.2", "out.3", "out.4"), each = length(thetas)), max = rep(c(max1, max2, max3, max4), each = length(thetas)))
ggplot(dat1) +
  geom_line(aes(x = thetas, y = ll)) +
  geom_vline(aes(xintercept = max), color = "red") +
  theme_minimal() +
  facet_wrap(~ type)
```

### Part e

These are the timings using Rmkl: 

<img title="rmkl comparison" src="./output/rmkl.png">

The codes run through much faster.

## Problem 2

I only reproduce and compare results of the parallelized bootols C-function, because I only parallize this function. I do not parallize ols and thus exclude them in this analysis (although they are of course employed for computing the results). To be able compare results from OMP and non-OMP side by side, I wrote a separate function `bootols_omp`, which is parallelized using `#pragma omp parallel`. I also wrote a separate R interface. Because compiling the Rmd uses up all RAM, I executed the commands in a separate session and included screeenshots of the results. This of course is obviously a quick-fix and could be avoided. Further, my implementation of the code is not thread-safe. I have not yet implemented that.

```{r, eval=FALSE}
source("./R/bootreg.R")
dyn.load("./src/clect_bootreg.so")

## generate data - smaller for bootstrap
n <- 5000
d <- 200
X <- 1/matrix(rt(n*d, df=1), ncol=d)
beta <- c(1:d, 0)
Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3)

## testing bootstrapped OLS
system.time(boot.Rlm <- bootols.R(X, Y, B=1000, method="lm"))
system.time(boot.Rinv <- bootols.R(X, Y, B=1000, method="inv"))
system.time(beta.Rsolve <- bootols.R(X, Y, B=1000, method="solve"))
system.time(boot.Cinv <- bootols(X, Y, B=1000, inv=TRUE))
system.time(beta.Csolve <- bootols(X, Y, B=1000))
system.time(boot.Cinv.omp <- bootols_omp(X, Y, B=1000, inv=TRUE))
system.time(beta.Csolve.omp <- bootols_omp(X, Y, B=1000))
```

<img title="bootols comparison" src="./output/bootols.png">

## Problem 3

First, I define the swap.eval function from last time together with, v, i, and j (i. e. I swap entries 1 and 2).
```{r}
v <- 1:1000000000
i <- 1
j <- 2
```
```{r}
swap.eval <- function()
  {
    e <- quote({tmp <- v[i]; v[i] <- v[j]; v[j] <- tmp})
    eval(e, envir=parent.frame())
}
```
Now, these Rcpp functions swap entries i and j without copying the whole vector. `swap_cpp_ptr` works with pointers. 
```{r}
library(Rcpp)
## Modifies v directly
cppFunction("
void swap_cpp(IntegerVector v, int i, int j){
  int tmp;
  tmp = v(i-1);
  v(i-1) = v(j-1);
  v(j-1) = tmp;
}
")
cppFunction("
void swap_cpp_ptr(IntegerVector v, int i, int j){
  int* ptri = &v(i-1); // pointer to i
  int* ptrj = &v(j-1); // pointer to j
  int tmp = *ptri;
  *ptri = *ptrj;
  *ptrj = tmp;
}
")
```

Now the timing:
```{r}
rbind(system.time(swap.eval()), system.time(swap.eval()), system.time(swap.eval()))
rbind(system.time(swap_cpp(v, i, j)), system.time(swap_cpp(v, i, j)), system.time(swap_cpp(v, i, j)))
rbind(system.time(swap_cpp_ptr(v, i, j)), system.time(swap_cpp_ptr(v, i, j)), system.time(swap_cpp_ptr(v, i, j)))
```

`swap.eval()` needs some time the first time it runs, then it runs almost  instantaneously. Both `swap_cpp` and `swap_cpp_ptr` run almost instantaneously in all three runs.

## Problem 4

I source the Cpp code from
```{r, eval=FALSE}
library(Rcpp)
sourceCpp("./src/loglik2.cpp")
```
Again, for comparison, I compute ll with the loglik2 function, and then I compare it with the Rcpp of logliks2 version (*Note:* I thought the task was to write Cpp code for `logliks2`, so that's why I did not code logliks.R as a C function, which would probably be better in general. Especially, because that would make e.g. parallelization easier. Further, I did include the `Machine$double.eps` from R by hand into the cpp code. This, I expect, led to the very small differences of the returned values):
```{r, eval=FALSE}
system.time( out.2 <- logliks.R(Y, D, thetas, ll=loglik2) )
system.time( out.rcpp <- logliks.R(Y, D, thetas, ll=loglik2cpp ) )
```
The Rcpp version is faster than the original version, but still not very fast. There is probably a lot of room for improvement and my implementation is not very efficient. The main problem is that I concentrated on translating the original function line by line, which was a bad strategy looking back.
<img title="logliks comparison" src="./output/logliks2cpp.png">

To demonstrate that the code yields (almost exactly the same ):
Both yield the same value for the maximum theta:
```{r}
(maxr <- thetas[which.max(out.4)])
(maxcpp <- thetas[which.max(out.rcpp)])
```

```{r,  dev.args = list(bg = 'transparent'), fig.width=4.5, fig.height=4, fig.align="center", no.main=TRUE}
dat <- data.frame(thetas = rep(thetas, 2), ll = c(out.4, out.rcpp), type = rep(c("loglik2", "loglik2.cpp"), each = length(thetas)), max = rep(c(maxr, maxcpp), each = length(thetas)))
ggplot(dat) +
  geom_line(aes(x = thetas, y = ll)) +
  geom_vline(aes(xintercept = max), color = "red") +
  theme_minimal() +
  facet_wrap(~ type)
```

```{r, include=FALSE, eval=FALSE}
save(Y, D, thetas, out.1, out.2, out.3, out.4, out.rcpp, file = "./output/rmd_out.rda")
```