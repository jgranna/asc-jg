#!/bin/bash
file=$1

# '.\{12,\}': Must be at least 12 characters long
# '/[0-9]/': Must contain at least one digit (0-9)
# '/[A-Z\s]+/': Must contain at least one upper case letter (A-Z)
# '/[a-z\s]+/': Must contain at least one lower case letter (a-z)
# '/[!@#$%^&]/': Must contain one of the following special characters !, @, #, $, %, ^, and &
# '/^[A-Za-z!@#$%^&0-9]+$/': Must only contain digits, upper case, lower case, and the above special characters

# awk '/[!@#$%^&]/' $1 | awk '/[0-9]/' | awk '/[A-Z\s]+/' | awk '/[a-z\s]+/'
# awk '/^[A-Za-z!@#$%^&0-9]+$/' $1 | awk '/[!@#$%^&]/' | awk '/[0-9]/' | awk '/[A-Z\s]+/' | awk '/[a-z\s]+/' | awk '/.{12,}/'
awk '/^[A-Za-z!@#$%^&0-9]+$/' $1 | awk '/[!@#$%^&]/' | awk '/[0-9]/' | awk '/[A-Z\s]+/' | awk '/[a-z\s]+/' | grep '.\{12,\}'
