---
title: "Homework 3"
author: "Julian Granna"
date: "8/10/2021"
output: html_document
---

```{r setup, include=FALSE}
# load libraries
library(knitr)
library(boot)
# set up options
knitr::opts_chunk$set(echo = TRUE)
# remove white margin above graphs
knit_hooks$set(no.main = function(before, options, envir) {
    if (before) par(mar = c(4.1, 4.1, 1.1, 1.1))  # smaller margin on top
})
## ```{r, dev.args = list(bg = 'transparent'), fig.width=4.5, fig.height=4, fig.align="center", no.main=TRUE}
```

## Problem 1
First, I enter all introduced functions: 
```{r}
powers1 <- function(x, dg)
  {
    pw <- matrix(x, nrow=length(x))
    prod <- x 
    for(i in 2:dg) {
      prod <- prod * x
      pw <- cbind(pw, prod)
    }
    return(pw)
  }
powers2 <- function(x, dg)
  {
    pw <- matrix(nrow=length(x), ncol=dg)
    prod <- x ## current product
    pw[,1] <- prod
    for(i in 2:dg) {
      prod <- prod * x
      pw[,i] <- prod
    }
  }
powers3 <- function(x, dg) outer(x, 1:dg, "^")
powers4 <- function(x, dg) 
  {
    repx <- matrix(rep(x, dg), nrow=length(x))
    return(t(apply(repx, 1, cumprod)))
  }
```
### Part a.  
The function `powers3()` calls the `outer()`-function with arguments `x`, `1:dg`, and `^`. Thereby, the function `^` is called to compute each element of the resulting matrix. Thus, the first column refers to vector `x` to the power 1, the second column refers to vector `x` to the power 2, ...  
Function `powers4()` first generates a matrix with `length(x)` rows and fills its columns with the `x` vector. Then, using the `apply()` function, it computes the cumulative product of each row. Since there are as many columns as degrees of power, the result is a dg x length(x) matrix filled with the powers of the vector. Finally, when transposed, the function returns the desired output.

### Part b.  
First, I define x analogously to the lecture slides:
```{r}
set.seed(051021)
x <- runif(1000000)
```
```{r, include=FALSE, cache=TRUE}
p1 <- powers1(x, 16)
p2 <- powers2(x, 16)
p3 <- powers3(x, 16)
p4 <- powers4(x, 16)
```
Then, the computation time of each function is given by (although could also be obtained from detailed profiling)
```{r, cache=TRUE}
system.time(p1 <- powers1(x, 16))
system.time(p2 <- powers2(x, 16))
system.time(p3 <- powers3(x, 16))
system.time(p4 <- powers4(x, 16))
```
The original two functions, `powers1` and `powers2` run faster than the added functions `powers3` and `powers4`.  

### Part c.
Now, I provide a summary of the profiles of computation time and memory usage. Note: Both `Rprof()` and `Rprofmem()` were run in separate R sessions, and are not executed while compiling this .Rmd file.
```{r, eval=FALSE}
Rprof("files/tp_p1.out")
p1 <- powers1(x, 16)
Rprof(NULL)
Rprofmem("files/memp_p1.out")
p1 <- powers1(x, 16)
Rprofmem(NULL)
```
The summares are given by:
```{r}
summaryRprof("files/tp_p1.out")
noquote(readLines("files/memp_p1.out", n = 5))
```
`powers2()`:
```{r, eval=FALSE}
Rprof("files/tp_p2.out")
p2 <- powers2(x, 16)
Rprof(NULL)
Rprofmem("files/memp_p2.out")
p2 <- powers2(x, 16)
Rprofmem(NULL)
```
The summaries are given by:
```{r}
summaryRprof("files/tp_p2.out")
noquote(readLines("files/memp_p2.out", n = 5))
```
`powers3()`:
```{r, eval=FALSE}
Rprof("files/tp_p3.out")
p3 <- powers3(x, 16)
Rprof(NULL)
Rprofmem("files/memp_p3.out")
p3 <- powers3(x, 16)
Rprofmem(NULL)
```
The summaries are given by:
```{r}
summaryRprof("files/tp_p3.out")
noquote(readLines("files/memp_p3.out", n = 5))
```
`powers4()`:
```{r, eval=FALSE}
Rprof("files/tp_p4.out")
p4 <- powers4(x, 16)
Rprof(NULL)
Rprofmem("files/memp_p4.out")
p4 <- powers4(x, 16)
Rprofmem(NULL)
```
The summaries are given by:
```{r}
summaryRprof("files/tp_p4.out")
noquote(readLines("files/memp_p4.out", n = 5))
```

As stated before, `powers2()` is the fastest of all functios followed by `powers1()`. One problem of the latter function is, as already stated in the slides, that the matrix dimensions are not correctly pre-specified meaning that with each step in the for loop, a new matrix is defined with an additional column. This is shown by the fact that `cbind()` takes up a lot of time.  
The time and memory summary of `power3()` are not very informative. It seems that the `outer()` function just takes a long time to run. It is a very *neat* code in the sense that it is very short and ease to understand, but the underlying algorithm does not seem efficient.  
Finally, `powers4()` is the slowest of all functions. When looking at the time summary, it is apparent that especially the `apply()` function is quite time consuming. Again, the `apply()` function is great for interpretability of the code, but in this case, it costs a substantial amount of time. In terms of memory consumption, especially the `matrix()` function seems costly.

## Problem 2
For the case that $S \sim N(10.5, 1)$ and $A \sim N(11, 1.5)$, we need to draw from a normal instead of a uniform distribution:
```{r}
n <- 1000
S <- rnorm(n, 10.5, 1)
A <- rnorm(n, 11, 1.5)
(prob <- mean(A < S))
se <- sqrt(prob*(1-prob)/n)
```
Then the estimate plus corresponding 95% confidence intervals is given by
```{r}
prob + c(-1,1)*1.96*se
```
And then plot
```{r, dev.args = list(bg = 'transparent'), fig.width=4.5, fig.height=4, fig.align="center", no.main=TRUE}
col <- A < S
plot(S[1:1000], A[1:1000], col = col + 1, xlab = "Sam", ylab = "Annie")
legend("topleft", c("A >= S", "A < S"), col = 1:2, pch = 1)
abline(h = 11, v = 10.5, lty = 2)
```
The code for the adjusted difference would not need to be adjusted:
```{r}
diff <- A - S
d <- c(mean(diff), mean(abs(diff)))
se <- c(sd(diff), sd(abs(diff)))/sqrt(n)
d[1] + c(-1,1)*1.96*se[1]
d[2] + c(-1,1)*1.96*se[2]
```

## Problem 3
I reproduce the data from the exercise in the slides:
```{r}
set.seed(510212)
n <- 200
X <- 1/cbind(rt(n, df=1), rt(n, df=1), rt(n,df=1))
beta <- c(1,2,3,0)
Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3)
```
And I further take the code from the slides for comparison later:
```{r, cache=TRUE}
B <- 10000
beta.hat.boot <- matrix(NA, nrow=B, ncol=length(beta))
system.time(  
  for(b in 1:B) {
    indices <- sample(1:nrow(X), nrow(X), replace=TRUE)
    beta.hat.boot[b,] <- coef(lm(Y[indices]~X[indices,]))
  }
)
```
Now, I use the `boot()` function:
```{r, cache=TRUE}
## I bind Y and X for easier input into boot() function
data <- cbind(Y, X)
## I define a function that boot() later calls
## It has "index" in it, so that boot() can draw bootstrap samples; otherwise
##  results in an error.
lm_fun <- function(data, index) 
  {
  d <- data[index,]
  return(coef(lm(d[, 1] ~ d[, -1])))
}
system.time(
  beta.boot.lib <- boot(data = data, statistic = lm_fun, R = 10000)
)
```
We can get a short summary of the results:
```{r}
beta.boot.lib
```
And I can compare the means as well as the quantiles of the results:
```{r}
## means of bootstrap estimates:
colMeans(beta.boot.lib$t)
colMeans(beta.hat.boot)
## 95% intervals
sapply(1:4, function(x) quantile(beta.boot.lib$t[, x], probs = c(.025, .975)))
sapply(1:4, function(x) quantile(beta.hat.boot[, x], probs = c(.025, .975)))
```
The coefficients and their corresponding intervals are, little surprising, almost identical. The code from class runs more than a second faster than `boot()`, which is not much in absoulte terms, but could perhaps make a difference for more complex problems.

## Problem 4  
The solution to Problem 4 is contained in `spam_mc.sh`. It takes one argument that sets the number of parallel processes (of the `spam_mc.R` script) that are run in the background. If no argument is specified, then it takes the number of (hyper-threaded) cores minus one. If the argument is greater than the number of cores of the machine, then the script executes again *number of cores - 1* processes. Right now, the script also runs `spam_mc.R` with `rep=1` and `seed` from 1 to the total number of processes. One could of course easily implement further arguments to alter these options. 

The following results are from running the `spam_mc.R` script in 15 parallel processes (i. e. command `./spam_mc.sh 15`) with 1 repetition each, corresponding to 15*1=15 repetitions (trivial, but indicating that more repetitions per process would be possible of course, too). 
```{r, echo=FALSE}
source("spam_mc_collect.R")
```
The results can be shown in a box plot, analogously to the lecture:  

```{r, echo=FALSE}
boxplot(df[,-1], main = "Problem 4: Cross Validation Hit Rates", xlab = "models", 
        ylab = "percentage of correct classification")
```

I do not provide the paired t test statistics, yet, because here, we only have $n = 15$. The paired t test statistics are provided later.   

## Problem 5
For Problem 5, there is an `.R`-script `spam_snow.R`, which runs, on default, 4 repititions with 5-fold cross validation running 4 instances in parallel using the `parallel`-package. The folder further contains the bash script `spam_snow.sh`, which takes three arguments: The first argument specifies the number of parallel instances, the second argument defines the number of cv folds and the third argument indicates the number of repititions. The results below were obtained by running 5 parallel instances of 3 repititions each with 5-fold cross-validation (5*3=15 reps in total).
```{r}
boxplot(df_snow[,-1], main = "Problem 5: Cross Validation Hit Rates", xlab = "models", 
        ylab = "percentage of correct classification")
```

## Combined results  
Finally, I provide the t tests for the combined 15+15=30 repititions:

```{r}
cbind(data.frame(dfmean), data.frame(tt))[o,]
```
Again, similar to the results obtained in the lecture, the random forest significantly outperforms the other models.  
