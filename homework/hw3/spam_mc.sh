#!/bin/bash

## first, grep number of cores available on machine
ncores=$(expr `grep -c ^processor /proc/cpuinfo`)

## check if argument is defined; otherwise use default 
if [ -n "$1" ]; then 
	
	## nested sanity check whether entered argument is larger than number of cores
	if (($1 > $ncores)); then
		
		c=$(expr $ncores - 1)
		echo "Number of chosen cores > than number of cores of machine. Run on " $c " cores."
		
	else
	
		c=$1
		echo "Run " $c " parallel R scripts."
		
	fi

## if no argument is passed, run (number of cores of machine - 1) processes
else
	c=$(expr $ncores - 1)
	echo "No parameters found. Run " $c " parallel R scripts."

fi

## run script c times
for ((n=(1+14);n<=($c+14);n++))
do 
	echo $n
	## seed is equal to variable from for loop
	R CMD BATCH '--args seed='"$n"' reps=1' spam_mc.R spam_mc_${n}.Rout &

done

echo Processes started in background.
