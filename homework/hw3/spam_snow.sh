#!/bin/bash

## first, grep number of cores available on machine
ncores=$(expr `grep -c ^processor /proc/cpuinfo`)

## check if argument is defined; otherwise use default 
if [ -n "$1" ]; then 
	
	## nested sanity check whether entered argument is larger than number of cores
	if (($1 > $ncores)); then
		
		## run on (ncores - 1), if argument is larger than no of cores on machine
		c=$(expr $ncores - 1)
		echo "Number of chosen cores > than number of cores of machine. Run on " $c " cores."
		
	else
	
		c=$1
		echo "Run " $c " parallel R scripts."
		
	fi

## if no argument is passed, run (number of cores of machine - 1) processes
else
	c=$(expr $ncores - 1)
	echo "No parameters found. Run " $c " parallel R scripts."

fi

## execute snow script with given arguments
R CMD BATCH '--args ninst='"$c"' cv.folds='"$2"' nreps='"$3"'' spam_snow.R spam_snow.Rout &

echo Processes with nreps = "$3", cv.folds = "$2" started in background in "$1" parallel instances.
