## find the spam saved outputs in the CWD
files <- list.files("./results/problem_4/")

## empty initialization
pfulls <- pfwds <- pfwdis <- pldas <- pqdas <- NULL
pnulls <- pfdas <- prps <- pmnlms <- prfs <- pfulls 

## loop over found RData files
for (i in 1:length(files))
{
  ## read file i and print to screen
  f <- paste0("results/problem_4/", files[i])
  load(f)
  cat(" ", f, sep="")

  ## bind files
  pnulls <- rbind(pnulls, pnull)
  pfulls <- rbind(pfulls, pfull)
  pfwds <- rbind(pfwds, pfwd)
  pfwdis <- rbind(pfwdis, pfwdi)
  pldas <- rbind(pldas, plda)
  pqdas <- rbind(pqdas, pqda)
  pfdas <- rbind(pfdas, pfda)
  prps <- rbind(prps, prp)
  prfs <- rbind(prfs, prf)
}

cat("\n")

spam <- read.csv("spam.csv")

hit <- function(x, y=spam$spam)
  mean(x == y, na.rm=TRUE)
df <- data.frame(null=apply(pnulls, 1, hit),
                 full=apply(pfulls, 1, hit),
                 fwd=apply(pfwds, 1, hit),
                 fwdi=apply(pfwdis, 1, hit),
                 lda=apply(pldas, 1, hit),
                 qda=apply(pqdas, 1, hit),
                 fda=apply(pfdas, 1, hit),
                 rp=apply(prps, 1, hit),
                 rf=apply(prfs, 1, hit))

## display ordered results with t-test info
# cbind(data.frame(dfmean), data.frame(tt))[o,]

load("results/problem_5/results.rda")
df_snow <- data.frame(null=apply(results$pnull, 1, hit),
                 full=apply(results$pfull, 1, hit),
                 fwd=apply(results$pfwd, 1, hit),
                 fwdi=apply(results$pfwdi, 1, hit),
                 lda=apply(results$plda, 1, hit),
                 qda=apply(results$pqda, 1, hit),
                 fda=apply(results$pfda, 1, hit),
                 rp=apply(results$prp, 1, hit),
                 rf=apply(results$prf, 1, hit))

## merge df and df_snow and apply t.test()
## order by averages and augment table with 
## paired t-test
df_comb <- rbind(df, df_snow)
dfmean <- apply(df_comb, 2, mean)
o <- order(dfmean, decreasing=TRUE)
tt <- rep(NA, length(dfmean))
for(i in 1:(length(o)-1)) {
  tto <- t.test(df[,o[i]], df[,o[i+1]], alternative="greater", paired=TRUE)
  tt[o[i]] <- tto$p.value
}
