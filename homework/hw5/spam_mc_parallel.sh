#!/bin/bash

#SBATCH -N 5
#SBATCH --ntasks-per-node=24
#SBATCH -t 35:00:00       
#SBATCH -p normal_q              
#SBATCH -A ascclass

module purge
module load parallel
module load intel/18.2 mkl R/3.6.2

#set r libraries
export R_LIBS="$HOME/cascades/R/3.6.2/intel/18.2/lib:$R_LIBS"

#set number of cores used by each r process
export MKL_NUM_THREADS=4

#number of r processes to run
ncopies=30

#processes to run at a time
nparallel=6

scontrol show hostname ${SLURM_JOB_NODELIST} > ./node_list

echo "$( date ): Starting spam_mc"

# for i in $( seq 1 $ncopies ); do 
#   R CMD BATCH "--args seed=$i reps=5" spam_mc.R spam_mc_${i}.Rout &
# done
# wait seq 1 $ncopies | parallel --env R --sshloginfile ./node_list --workdir $SLURM_SUBMIT_DIR -j$nparallel "R CMD BATCH \"--args seed={} reps=5\" spam_mc.R spam_mc_{}.Rout"
export PARALLEL="--workdir . --env PATH --env LD_LIBRARY_PATH --env LOADEDMODULES --env _LMFILES_ --env MODULE_VERSION --env MODULEPATH --env MODULEVERSION_STACK --env MODULESHOME --env OMP_DYNAMICS --env OMP_MAX_ACTIVE_LEVELS --env OMP_NESTED --env OMP_NUM_THREADS --env OMP_SCHEDULE --env OMP_STACKSIZE --env OMP_THREAD_LIMIT --env OMP_WAIT_POLICY"
seq 1 $ncopies | parallel --sshloginfile $PBS_NODEFILE --env R --workdir $SLURM_SUBMIT_DIR -j$nparallel "R CMD BATCH \"--args seed={} reps=1\" spam_mc.R spam_mc_{}.Rout"

echo "$( date ): Finished spam_mc"


echo "$( date ): Starting spam_mc_collect"
R CMD BATCH spam_mc_collect.R spam_mc_collect.Rout
echo "$( date ): Finished spam_mc_collect"

